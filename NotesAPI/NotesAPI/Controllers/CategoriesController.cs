﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NotesAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private static readonly List<Category> _categories = new List<Category>()
        {
            new Category() { Id = "1", Name = "To Do" },
            new Category() { Id = "2", Name = "Doing" },
            new Category() { Id = "3", Name = "Done" }
        };

        /// <summary>
        /// Get one category based on the corresponding id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>The category with the corresponding id</returns>
        
        [HttpGet("{id}", Name = "GetCategory")]
        public IActionResult GetCategory(string id)
        {
            var category = _categories.Where(categ => categ.Id == id);

            if (category == null)
            {
                return NotFound($"The category with the id equal to {id} has not been found!");
            }
            return Ok(category);
        }

        /// <summary>
        /// Get the list of all categories
        /// </summary>
        /// <returns>List of all categories</returns>
        
        [HttpGet]
        public IActionResult GetCategories()
        {
            return Ok(_categories);
        }

        /// <summary>
        /// Add a new category to the existing list of categories
        /// </summary>
        /// <param name="category"></param>
        
        [HttpPost]
        public IActionResult Post([FromBody] Category category)
        {
            _categories.Add(category);
            //return Ok("Note has been successfully added!");
            return CreatedAtRoute("GetCategory", new { id = category.Id.ToString() }, category);
        }

        /// <summary>
        /// Delete one category from the list of categories
        /// </summary>
        /// <param name="id"></param>
        
        [HttpDelete("{id}")]
        public IActionResult Delete(string id)
        {
            var category = _categories.FirstOrDefault(categ => categ.Id == id);
            
            if (category == null)
            {
                return NotFound($"The category with the id equal to {id} has not been found!");
            }

            _categories.Remove(category);
            return Ok("Category has been removed!");
        }
    }
}
