﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using NotesAPI.Models;
using NotesAPI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class OwnerController : ControllerBase
    {
        IOwnerCollectionService _ownerCollectionService;

        public OwnerController(IOwnerCollectionService ownerCollectionService)
        {
            _ownerCollectionService = ownerCollectionService ?? throw new ArgumentNullException(nameof(ownerCollectionService));
        }


        /// <summary>
        /// Get one owner based on the corresponding id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>The owner with the corresponding id</returns>

        [HttpGet("{id}")]
        public async Task<IActionResult> GetOne(Guid id)
        {
            try
            {
                return Ok(await _ownerCollectionService.Get(id));
            }
            catch (Exception e)
            {
                return NotFound($"The owner with the id equal to {id} has not been found!");
            }
        }

        /// <summary>
        /// Get the list of all owners
        /// </summary>
        /// <returns>List of all owners</returns>

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            return Ok(await _ownerCollectionService.GetAll());
        }

        /// <summary>
        /// Add a new owner to the list of owners
        /// </summary>
        /// <param name="owner"></param>

        [HttpPost]
        public async Task<IActionResult> CreateOwner([FromBody] Owner owner)
        {
            if (!await _ownerCollectionService.Create(owner))
            {
                return BadRequest("Owner can't be null");
            }

            return Ok(await _ownerCollectionService.GetAll());
        }

        /// <summary>
        /// Update the owner with a certain ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="owner"></param>

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateOwner(Guid id, [FromBody] Owner owner)
        {
            if (owner == null)
            {
                return BadRequest("Owner cannot be null!");
            }

            if (!await _ownerCollectionService.Update(id, owner))
            {
                return NotFound($"The owner with the id equal to {id} has not been found!");
            }

            return Ok(await _ownerCollectionService.GetAll());
        }

        /// <summary>
        /// Delete an owner with a certain ID
        /// </summary>
        /// <param name="id"></param>

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOwner(Guid id)
        {
            if (!await _ownerCollectionService.Delete(id))
            {
                return NotFound("The owner doesn't exist!");
            }

            return Ok(await _ownerCollectionService.GetAll());
        }
    }
}
