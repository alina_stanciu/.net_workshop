﻿using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;
using NotesAPI.Models;
using NotesAPI.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotesAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class NotesController : ControllerBase
    {
        INoteCollectionService _noteCollectionService;
        
        public NotesController(INoteCollectionService noteCollectionService) 
        {
            _noteCollectionService = noteCollectionService ?? throw new ArgumentNullException(nameof(noteCollectionService));
        }


        /// <summary>
        /// Get one note based on the corresponding ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns>The note with the corresponding id</returns>

        [HttpGet("{id}")]
        public async Task<IActionResult> GetNoteById(Guid id)
        {
            try
            {
                return Ok(await _noteCollectionService.Get(id));
            }
            catch (Exception e)
            {
                return NotFound($"The note with the id equal to {id} has not been found!");
            }
        }

        /// <summary>
        /// Get the list of all notes
        /// </summary>
        /// <returns>List of all notes</returns>

        [HttpGet]
        public async Task<IActionResult> GetAllNotes()
        {
            return Ok(await _noteCollectionService.GetAll());
        }

        /// <summary>
        /// Get all notes of a certain owner
        /// </summary>
        /// <param name="id"></param>
        /// <returns>The note with the corresponding owner's id</returns>

        [HttpGet("owner:{id}")]
        public async Task<IActionResult> GetNoteByOwnerId(Guid id)
        {
            try
            {
                return Ok(await _noteCollectionService.GetNotesByOwnerId(id));
            }
            catch (Exception e)
            {
                return NotFound($"The note with the owner's id equal to {id} has not been found!");
            }
        }

        /// <summary>
        /// Add a new note to the list of notes
        /// </summary>
        /// <param name="note"></param>
        /// <returns>The list of notes with the newly added note</returns>

        [HttpPost]
        public async Task<IActionResult> CreateNote([FromBody] Note note)
        {
            if (note == null)
            {
                return BadRequest("Note cannot be null!");
            }
            await _noteCollectionService.Create(note);

            return Ok(await _noteCollectionService.GetAll());
        }

        /// <summary>
        /// Update the note with a certain ID
        /// </summary>
        /// <param name = "id" ></param>
        /// <param name = "note" ></param>

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateNote(Guid id, [FromBody] Note note)
        {
            if (note == null)
            {
                return BadRequest("Note cannot be null!");
            }

            if (!await _noteCollectionService.Update(id, note))
            {
                return NotFound($"The note with the id equal to {id} has not been found!");
            }
            return Ok(await _noteCollectionService.GetAll());
        }

        /// <summary>
        /// Update note based on the IDs of both the owner and the note
        /// </summary>
        /// <param name="idOwner"></param>
        /// <param name="idNote"></param>
        /// <param name="note"></param>

        [HttpPut("{idOwner}/{idNote}")]
        public async Task<IActionResult> UpdateNoteAdvanced(Guid idOwner, Guid idNote, [FromBody] Note note)
        {
            if (note == null)
            {
                return BadRequest("The updated note cannot be null!");
            }

            if (!await _noteCollectionService.UpdateAdvanced(idOwner, idNote, note))
            {
                return NotFound("The note doesn't exist!");
            }

            return Ok(await _noteCollectionService.GetAll());
        }

        /// <summary>
        /// Delete a note with a certain ID
        /// </summary>
        /// <param name="id"></param>

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteNote(Guid id)
        {
            if (!await _noteCollectionService.Delete(id))
            {
                return NotFound("The note doesn't exist!");
            }

            return Ok(await _noteCollectionService.GetAll());
        }

        /// <summary>
        /// Delete note based on the IDs of both the owner and the note
        /// </summary>
        /// <param name="idOwner"></param>
        /// <param name="idNote"></param>

        [HttpDelete("{idOwner}/{idNote}")]
        public async Task<IActionResult> DeleteNoteAdvanced(Guid idOwner, Guid idNote)
        {
            if (!await _noteCollectionService.DeleteNoteAdvanced(idOwner, idNote))
            {
                return NotFound("The note doesn't exist!");
            }

            return Ok(await _noteCollectionService.GetAll());
        }

        /// <summary>
        /// Delete all notes of an owner with a specifed ID
        /// </summary>
        /// <param name="id"></param>

        [HttpDelete("owner:{id}")]
        public async Task<IActionResult> DeleteAllNotesOfOwner(Guid id)
        {
            if (!await _noteCollectionService.DeleteAllNotesOfOwner(id))
            {
                return NotFound("The specifed owner doesn't have any notes!");
            }

            return Ok(await _noteCollectionService.GetAll());
        }
    }
}
